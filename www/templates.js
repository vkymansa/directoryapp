(function() {
window["JST"] = window["JST"] || {};

window["JST"]["index.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<!DOCTYPE html>\n<html>\n<head>\n\t<title>Directory</title>\n<!--Jquery-->\n<!--vendor-->\n<script type="text/javascript" src="lib/vendor.min.js"></script>\n<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">\n<!--Stylesheets-->\n<link rel="stylesheet" type="text/css" href="css/style.min.css">\n<!--app-->\n<script type="text/javascript" src="js/app/app.min.js"></script>\n</head>\n<body ng-app="directory">\n<div class="container-fluid">\n<div ng-view class="view-animate"></div>\n</div>\n\n</body>\n</html>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["details/details.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="page-header">\n\t<a href="#/back" class="fl back-btn">\n          &nbsp;&nbsp;<span class="glyphicon glyphicon-chevron-left"></span>\n\t</a>\n\t<h3>Person Details</h3>\n</div>\n<div class="ma details" ng-repeat="detail in details | filter: { id: id }:true">\n<li class="list">\n\t\t\t<div class="col-sm-1"><img ng-src="{{detail.img}}" class="img-circle img-responsive icon"></div>\n\t\t\t<div class="col-sm-11"><p align="left"><strong>{{detail.name}}</strong><br/>{{detail.designation}}</p></div>\n\t\t</li>\n\t\t<a href="tel:{{detail.office}}">\n\t\t<li class="list">\n\t\t\t<div class="col-sm-11"><p align="left"><strong>Call Office</strong><br/>{{detail.office}}</p></div>\n\t\t\t<div class="col-sm-1">\n\t\t\t\t<p align="right"><span class="glyphicon glyphicon-chevron-right"></span></p>\n\t\t\t</div>\n\t\t</li>\n\t\t</a>\n\t\t<a href="tel:{{detail.mobile}}">\n\t\t<li class="list">\n\t\t\t<div class="col-sm-11"><p align="left"><strong>Call Mobile</strong><br/>{{detail.mobile}}</p></div>\n\t\t\t<div class="col-sm-1">\n\t\t\t\t<p align="right"><span class="glyphicon glyphicon-chevron-right"></span></p>\n\t\t\t</div>\n\t\t</li>\n\t\t</a>\n\t\t<a href="sms:{{detail.sms}}">\n\t\t<li class="list">\n\t\t\t<div class="col-sm-11"><p align="left"><strong>SMS</strong><br/>{{detail.sms}}</p></div>\n\t\t\t<div class="col-sm-1">\n\t\t\t\t<p align="right"><span class="glyphicon glyphicon-chevron-right"></span></p>\n\t\t\t</div>\n\t\t</li>\n\t\t</a>\n\t\t<a href="mailto:{{detail.email}}">\n\t\t<li class="list">\n\t\t\t<div class="col-sm-11"><p align="left"><strong>Email</strong><br/>{{detail.email}}</p></div>\n\t\t\t<div class="col-sm-1">\n\t\t\t\t<p align="right"><span class="glyphicon glyphicon-chevron-right"></span></p>\n\t\t\t</div>\n\t\t</li>\n\t\t</a>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["index/index.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="page-header">\n\t<h3>Directory List</h3>\n</div>\n<div class="content">\n\t<div class="field">\n\t\t<input type="text" ng-model="name"  class="form-control"/>\n\t</div>\n\n</div>\n<ul class="animate-repeat"  ng-repeat="detail in details | filter: { name: name }">\n\t<a href="#details/{{detail.id}}">\n\t\t<li class="list">\n\t\t\t<div class="col-sm-1"><img ng-src="{{detail.img}}" class="img-circle img-responsive icon"></div>\n\t\t\t<div class="col-sm-11"><p align="left"><strong>{{detail.name}}</strong><br/>{{detail.designation}}</p></div>\n\t\t</li>\n\n\t</a>\n</ul>';

}
return __p
}})();